#!/bin/bash
# Install and run the UNICAMP VPN connection / configuration.
# Written by Hildo Guillardi Júnior on 08/April/2018.
# License GPLv3+

# Run  as common user
if (( $EUID == 0 )); then
	echo 'Run not as Super User'
	exit 1
fi
file_script=$(readlink -f $0)

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ICON="$DIR/logoUNICAMPvpn.png"

if [[ -z $1 ]] || [[ $1 != '--install' ]]; then
	# Run the connection.
	#dpkg -l | grep -q 'openvpn' || (echo '"openvpn" package not founded! Use `bash vpnunicamp --install` do download the package / reset the configurations.' && exit 1)
	dpkg -l | grep -q 'openvpn' || (echo '"openvpn" package not founded!' && bash -c $file_script --install && exit 1)
	dpkg -l | grep -q 'xterm' || (echo '"xterm" package not founded!' && bash -c $file_script --install && exit 1)
	
	CMD='echo -e "Use \`bash vpnunicamp.sh --install\` to (re)install or reset the configurations.\n" && '
	CMD+="echo -e \"To login into the VPN:\n1) Enter SUDO password\n2) Enter UNICAMP login (XXXXXX@unicamp.br)\n3) Press <Ctrl>+c to logout\" && sudo openvpn --config '$DIR/client.ovpn'"
	echo 'Connecting to UNICAMP VPN...'
	(xterm -T "UNICAMP VPN connection" -n "$ICON" -geometry 55x8 -e "$CMD" &&
	echo 'Disconnected from UNICAMP VPN!') ||
	echo 'Falied to connected to UNICAMP VPN!'

else
	# Install the required packages, create the configurations and shortcuts.
	shortcut="$HOME/Desktop/OpenVPN.desktop"
	_USER=$(basename ~)
	
	echo '1) Checking requirement installtion...'

	dpkg -l | grep openvpn -q &&
		echo -e '`openvpn` already installed!\n' ||
	(echo '`openvpn` not installed!'
	echo 'Installing `openvpn`...' &&
	sudo apt-get -y install openvpn &&
	echo -e '`openvpn` installed. \n')

	dpkg -l | grep xterm -q &&
		echo -e '`xterm` already installed!\n' ||
	(echo '`xterm` not installed!'
	echo 'Installing `xterm`...' &&
	sudo apt-get -y install xterm &&
	echo -e '`xterm` installed. \n')
	
	echo '2) Creating desktop shortcut...' &&
	#sudo -u $_USER 
	bash -c "cat >$shortcut <<EOF
[Desktop Entry]
Version=1.0
Exec=bash $file_script
Name=VPN UNICAMP
GenericName=SSH Server
Comment=Connect to Univesity VPN
Encoding=UTF-8
Terminal=false
Type=Application
Categories=Application;Network;
Icon=$ICON
EOF" &&
	chmod +x $shortcut &&
	echo -e 'Desktop shortcut created.\n'
	
	echo '3) Opening UNICAMP connection file key generation web site...' &&
	echo 'Use XXXXXX@unicamp.br as user name.'
	echo 'Save the "client.ovpn" file in the same folder of this script.' &&
	#xdg-open http://www.ccuec.unicamp.br/ccuec/material_apoio/tutorial_linux_vpn_nova_versao &&
	(xdg-open https://config.vpn.unicamp.br/ &) &&
	echo -e '\n'

fi

exit 0